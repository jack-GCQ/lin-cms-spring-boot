# Lin CMS

单元测试

权限管理
前后端一体化
CMS的接口需要权限验证

多个权限注解
@LoginRequired
     必须需要登录
@GroupRequired
    只有对应分组才能访问
@AdminRequired
    只有管理员才能使用
@PermissionMeta
    是否有指定权限 
@PermissionModule

双令牌机制
access_token 2小时
refresh_token 7天

access_token > refresh_token 

不需要输入账号密码 refresh_token就能换取到新的access_token
每次在获取access_token时 就会换取新的refresh_token


### JWT令牌安全性
前后分离 用户登录问题

单令牌机制
有效访问时间 7天
access_token
到期后，令牌验证过期 打扰到用户 体验不好


行为日志
    记录每个请求的日志
文件上传

项目规范

日志系统
1、系统日志
2、行为日志
令牌

扩展

架构

websocket模块


module 模块
extension 插件
松耦合
插件思想 分享给到其他开发者


JSR 注解
@NotBlank 必须传该字段
@Length min max 限制长度
@Positive 带注释的元素必须是严格的正数 必须是正整数


源码:

场景
场景没有经历到 3-5年经验




统一返回结果 UnifyResponse

条件
1、分页 page
2、条件查询返回结果
3、更新、删除返回结果

每一种状态返回的状态码？
成功 200？
更新 201 ？
删除 ？

设计思想是什么？


程序员综合素质
1、产品
2、角度
3、综合角度、运营、设计




# 前端
Vue3

响应式API
为了更加方便维护，相关功能都抽取成api
setup
ref
reactive
readonly
computed
watch

比 vue2 理解程度高点
写完方面改变

Vite
对比 Webpack 构建工具
比 webpack 更加快
兼容，要求相关依赖都是 vue3, 有待开发



### Vue-router



### 权限

用户、分组、权限



权限实质



权限粒度问题 查询

A能看到删除

B不能看到



动态拼接 SQL 查询字段



### Lin-cms

#### 1、router 配置

在 `config` 中直接配置对应页面的js文件，参考 book.js

```javascript
const bookRouter = {
  route: null,
  name: null,
  title: '图书管理',
  type: 'folder', // 类型: folder, tab, view【页面/舞台】
  icon: 'iconfont icon-tushuguanli',
  filePath: 'view/book/', // 文件路径
  order: null, // 是否需要打开新路由
  inNav: true,
  children: [
    {
      title: '图书列表', // 菜单标题
      type: 'view', // 打开类型
      name: 'BookCreate', // 路由名称
      route: '/book/list', // 路由地址
      filePath: 'view/book/book-list.vue', // 路由对应页面的地址
      inNav: true, // 是否显示
      icon: 'iconfont icon-tushuguanli', // 菜单图标
    }
    // 有多个则配置多个
  ],
}

export default bookRouter

```

扩展思考：

1、导入方式，采用多个 js 文件导入，避免单个文件过多



#### 2、UploadImg 图片上传组件

执行流程

上传图片，返回图片url

自动把图片插入到图片表中

