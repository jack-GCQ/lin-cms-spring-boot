package io.github.talelin.latticy.laver.model;

import lombok.Data;

import java.util.List;

/**
 * join 查询 关联
 * spu
 *  category_id  =>  category =>  categoryName
 *  sketch_spec_id  =>  spec => specName
 *  default_sku_id =>  sku => defaultSkuName
 * @author gcq
 * @Create 2021-12-05
 */
@Data
public class SpuDetailDO extends SpuDO{

    private String categoryName;

    private String sketchSpecName;

    private String defaultSkuName;

    private List<String> spuImgList;

    private List<String> spuDetailImgList;
}