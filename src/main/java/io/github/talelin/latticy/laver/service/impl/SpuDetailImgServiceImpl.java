package io.github.talelin.latticy.laver.service.impl;

import io.github.talelin.latticy.laver.model.SpuDetailImgDO;
import io.github.talelin.latticy.laver.service.SpuDetailImgService;
import io.github.talelin.latticy.laver.mapper.SpuDetailImgMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-07
 */
@Service
public class SpuDetailImgServiceImpl extends ServiceImpl<SpuDetailImgMapper, SpuDetailImgDO> implements SpuDetailImgService {

}
