package io.github.talelin.latticy.laver.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author gcq
 * @Create 2021-11-28
 */
@Getter
@Setter
public class BannerDTO {

    @NotBlank
    @Length(min = 2, max = 20)
    private String name;

    @NotBlank
    @Length(min = 2, max = 30)
    private String description;

    @NotBlank
    @Length(min = 2, max = 256)
    private String title;

    @NotBlank
    @Length(min = 2, max = 256)
    private String img;
}