package io.github.talelin.latticy.laver.service.impl;

import io.github.talelin.latticy.laver.model.SkuDO;
import io.github.talelin.latticy.laver.mapper.SkuMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.talelin.latticy.laver.service.SkuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-04
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, SkuDO> implements SkuService {

}
