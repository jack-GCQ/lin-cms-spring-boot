package io.github.talelin.latticy.laver.service.impl;

import io.github.talelin.core.util.BeanUtil;
import io.github.talelin.latticy.laver.dto.SpuDTO;
import io.github.talelin.latticy.laver.model.*;
import io.github.talelin.latticy.laver.mapper.SpuMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.talelin.latticy.laver.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-04
 */
@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, SpuDO> implements SpuService {
    @Autowired
    SpuMapper spuMapper;
    @Autowired
    CategoryService categoryService;
    @Autowired
    SpuImgService spuImgService;
    @Autowired
    SpuDetailImgService spuDetailImgService;
    @Autowired
    SpuKeyService spuKeyService;
    
    @Override
    public SpuDetailDO getDetail(Long id) {
        return spuMapper.getDetail(id);
    }

    @Override
    @Transactional
    public void create(SpuDTO dto) {
        SpuDO spuDO = new SpuDO();
        BeanUtils.copyProperties(dto, spuDO);
        CategoryDO category = categoryService.getCategory(dto.getCategoryId());
        spuDO.setCategoryId(category.getParentId());
        this.save(spuDO);

        List<String> spuImgList = new ArrayList<>();
        if(dto.getSpuImgList() == null)
        {
            spuImgList.add(dto.getImg());
        }
        else
        {
            spuImgList = dto.getSpuImgList();
        }
        Integer spuId = spuDO.getId();
        this.insertSpuImgList(spuImgList, spuId);
        if(dto.getDetailImgList() != null)
        {
            this.insertSpuDetailImgList(dto.getDetailImgList(), spuId);
        }

        if(dto.getSpecKeyIdList() != null)
        {
            this.insertSpuKeyList(dto.getSpecKeyIdList(), spuId);
        }
    }

    private void insertSpuImgList(List<String> spuImgList, Integer spuId)
    {
        List<SpuImgDO> spuImgDOList = spuImgList.stream().map(s -> {
            SpuImgDO spuImgDO = new SpuImgDO();
            spuImgDO.setImg(s);
            spuImgDO.setSpuId(spuId);
            return spuImgDO;
        }).collect(Collectors.toList());

        this.spuImgService.saveBatch(spuImgDOList);
    }

    private void insertSpuDetailImgList(List<String> SpuDetailImgList, Integer spuId)
    {
        List<SpuDetailImgDO> spuDetailImgDOList = new ArrayList<>();
        for (int i = 0; i < spuDetailImgDOList.size(); i++) {
            SpuDetailImgDO spuDetailImgDO = new SpuDetailImgDO();
            spuDetailImgDO.setImg(SpuDetailImgList.get(i))
                    .setSpuId(spuId)
                    .setIndex(i);
            spuDetailImgDOList.add(spuDetailImgDO);
        }
        this.spuDetailImgService.saveBatch(spuDetailImgDOList);
    }

    private void insertSpuKeyList(List<Integer> spuKeyList, Integer spuId)
    {
        List<SpuKeyDO> spuKeyDOList = spuKeyList.stream().map(s -> {
            SpuKeyDO spuKeyDO = new SpuKeyDO();
            spuKeyDO.setSpecKeyId(s)
                    .setSpuId(spuId);
            return spuKeyDO;
        }).collect(Collectors.toList());
        spuKeyService.saveBatch(spuKeyDOList);
    }
}
