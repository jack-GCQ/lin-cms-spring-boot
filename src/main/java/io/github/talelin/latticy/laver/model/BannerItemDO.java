package io.github.talelin.latticy.laver.model;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.github.talelin.latticy.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author gcq
 * @Create 2021-11-29
 */
@Getter
@Setter
@TableName("banner_item")
public class BannerItemDO{
    private Long id;
    private String img;
    private String keyword;
    private Integer type;
    private Long BannerId;

    @JsonIgnore
    private Date createTime;

    @JsonIgnore
    private Date updateTime;

    @TableLogic
    @JsonIgnore
    private Date deleteTime;
}