package io.github.talelin.latticy.laver.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author gcq
 * @Create 2021-12-01
 */
@Getter
@Setter
public class BannerDTO {
    @NotBlank
    @Length(min = 2, max = 20)
    private String name;

    @Length(min =2, max = 20)
    private String title;

    @Length(min =2, max = 20)
    private String img;

    @Length(min =2, max = 20)
    private String description;
}