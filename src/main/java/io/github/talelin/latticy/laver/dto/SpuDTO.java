package io.github.talelin.latticy.laver.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.List;

/**
 *  传入 5000 不存在
 *  @Positive 基础注解
 *  需要验证传入的id 是否是合法的
 * @author gcq
 * @Create 2021-12-06
 */
@Data
public class SpuDTO {

    @NotBlank
    @Length(min = 1, max = 128)
    private String title;

    @NotBlank
    @Length(min = 1, max = 255)
    private String subtitle;

    @NotBlank
    @Length(min = 1, max = 255)
    private String img;

    @NotBlank
    @Length(min = 1, max = 255)
    private String forThemeImg;

    @Positive
    @NotNull
    private Long categoryId;

    @Max(1)
    @Min(0)
    private Integer online;

    @Positive
    private Long sketchSpecId;


    @Positive
    private Long defaultSkuId;

    @Positive
    @Length(min = 1, max = 20)
    private String price;

    @Length(min = 1, max = 255)
    private String description;

    @Length(min = 1, max = 255)
    private String tags;

    private List<Integer> specKeyIdList;

    private List<String> spuImgList;

    private List<String> detailImgList;

}