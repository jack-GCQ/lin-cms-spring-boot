package io.github.talelin.latticy.laver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.talelin.latticy.laver.model.BannerDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * @author gcq
 * @Create 2021-11-26
 */
@Repository
public interface BannerMapper extends BaseMapper<BannerDO> {

}
