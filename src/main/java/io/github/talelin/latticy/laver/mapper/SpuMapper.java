package io.github.talelin.latticy.laver.mapper;

import io.github.talelin.latticy.laver.model.SpuDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.talelin.latticy.laver.model.SpuDetailDO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-04
 */
public interface SpuMapper extends BaseMapper<SpuDO> {

    SpuDetailDO getDetail(@Param("id") Long id);
}
