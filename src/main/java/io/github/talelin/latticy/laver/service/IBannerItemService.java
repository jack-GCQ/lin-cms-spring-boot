package io.github.talelin.latticy.laver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.github.talelin.latticy.laver.model.BannerDO;
import io.github.talelin.latticy.laver.model.BannerItemDO;

/**
 * @author gcq
 * @Create 2021-11-29
 */
public interface IBannerItemService extends IService<BannerItemDO> {

}