package io.github.talelin.latticy.laver.service.impl;

import io.github.talelin.autoconfigure.exception.NotFoundException;
import io.github.talelin.latticy.laver.service.CategoryService;
import io.github.talelin.latticy.laver.model.CategoryDO;
import io.github.talelin.latticy.laver.mapper.CategoryMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-07
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, CategoryDO> implements CategoryService {

    @Override
    public CategoryDO getCategory(Long id) {
        CategoryDO category = this.getById(id);
        if (category == null) {
            throw new NotFoundException(40000);
        }
        return category;
    }
}
