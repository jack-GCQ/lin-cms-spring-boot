package io.github.talelin.latticy.laver.mapper;

import io.github.talelin.latticy.laver.model.CategoryDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-07
 */
public interface CategoryMapper extends BaseMapper<CategoryDO> {

}
