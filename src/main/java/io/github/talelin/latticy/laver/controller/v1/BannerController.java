package io.github.talelin.latticy.laver.controller.v1;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.github.talelin.core.annotation.*;
import io.github.talelin.latticy.common.util.PageUtil;
import io.github.talelin.latticy.laver.bo.BannerWithItemsBO;
import io.github.talelin.latticy.laver.dto.BannerDTO;
import io.github.talelin.latticy.laver.model.BannerDO;
import io.github.talelin.latticy.laver.model.BannerItemDO;
import io.github.talelin.latticy.laver.service.IBannerService;
import io.github.talelin.latticy.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.util.List;

/**
 * Banner
 *
 *
 *
 * @author gcq
 * @Create 2021-11-27
 */
@RequestMapping("/v1/banner")
@RestController
@Validated
public class BannerController {

    @Autowired
    IBannerService bannerService;

    @PostMapping
    public CreatedVO create(@RequestBody @Validated BannerDTO bannerDTO)
    {
        BannerDO bannerDO = new BannerDO();
        BeanUtils.copyProperties(bannerDTO, bannerDO);
        Integer result = bannerService.create(bannerDO);
        return new CreatedVO(result);
    }


    @DeleteMapping("/{id}")
    @PermissionMeta(value = "删除Banner")
    @GroupRequired
    public DeletedVO delete(@PathVariable("id") Long id)
    {
        bannerService.deleteById(id);
        return new DeletedVO();
    }

    @GetMapping("/{id}")
    @LoginRequired
    @PermissionMeta(value = "查询Banner")
    @GroupRequired
    @Logger(template = "{user.username}查询了Banner数据")
    public UnifyResponseVO getWithItems(@PathVariable("id") Long id)
    {
        BannerWithItemsBO bannerWithItems = bannerService.getWithItems(id);
        return new UnifyResponseVO(bannerWithItems);
    }

    /**
     * 参数是否传递
     */
    @PutMapping("/{id}")
    public UpdatedVO update(@RequestBody @Validated BannerDTO bannerDTO,
                            @PathVariable @Positive Long id)
    {
        bannerService.update(id, bannerDTO);
        return new UpdatedVO("更新成功");
    }

    /**
     * start count 适合移动端
     * pc端 适合 page count
     * 使用 max min 注解效验参数
     */
    @GetMapping("/page")
    @LoginRequired
    public PageResponseVO<BannerDO> getBanners(@RequestParam(name = "page", defaultValue = "0", required = false)
                                               @Min(value = 0) Integer page,
                                               @RequestParam(name = "count", defaultValue = "10", required = false)
                                               @Min(value = 1)
                                               @Max(value = 30) Integer count)
    {
        IPage<BannerDO> banners = bannerService.getBanners(page, count);
        return PageUtil.build(banners);
    }
}