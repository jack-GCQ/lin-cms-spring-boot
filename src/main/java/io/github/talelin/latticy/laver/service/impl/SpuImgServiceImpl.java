package io.github.talelin.latticy.laver.service.impl;

import io.github.talelin.latticy.laver.model.SpuImgDO;
import io.github.talelin.latticy.laver.service.SpuImgService;
import io.github.talelin.latticy.laver.mapper.SpuImgMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-07
 */
@Service
public class SpuImgServiceImpl extends ServiceImpl<SpuImgMapper, SpuImgDO> implements SpuImgService {

}
