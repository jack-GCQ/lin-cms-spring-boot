package io.github.talelin.latticy.laver.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import io.github.talelin.latticy.laver.bo.BannerWithItemsBO;
import io.github.talelin.latticy.laver.dto.BannerDTO;
import io.github.talelin.latticy.laver.model.BannerDO;

/**
 * @author gcq
 * @Create 2021-11-27
 */
public interface IBannerService extends IService<BannerDO> {

    IPage<BannerDO> getBanners(Integer page, Integer count);

    void update(Long id, BannerDTO bannerDTO);

    void deleteById(Long id);

    BannerWithItemsBO getWithItems(Long id);

    Integer create(BannerDO bannerDO);

}
