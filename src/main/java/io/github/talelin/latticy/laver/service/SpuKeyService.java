package io.github.talelin.latticy.laver.service;

import io.github.talelin.latticy.laver.model.SpuKeyDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-07
 */
public interface SpuKeyService extends IService<SpuKeyDO> {

}
