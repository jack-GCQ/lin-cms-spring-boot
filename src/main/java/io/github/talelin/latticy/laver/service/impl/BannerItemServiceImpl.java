package io.github.talelin.latticy.laver.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.talelin.latticy.laver.mapper.BannerItemMapper;
import io.github.talelin.latticy.laver.model.BannerItemDO;
import io.github.talelin.latticy.laver.service.IBannerItemService;

/**
 * @author gcq
 * @Create 2021-11-29
 */
public class BannerItemServiceImpl extends ServiceImpl<BannerItemMapper, BannerItemDO> implements IBannerItemService {

}