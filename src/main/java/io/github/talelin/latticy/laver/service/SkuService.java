package io.github.talelin.latticy.laver.service;

import io.github.talelin.latticy.laver.model.SkuDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-04
 */
public interface SkuService extends IService<SkuDO> {

}
