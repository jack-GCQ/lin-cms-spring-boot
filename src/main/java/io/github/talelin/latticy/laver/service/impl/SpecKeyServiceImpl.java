package io.github.talelin.latticy.laver.service.impl;

import io.github.talelin.latticy.laver.service.SpecKeyService;
import io.github.talelin.latticy.model.SpecKeyDO;
import io.github.talelin.latticy.laver.mapper.SpecKeyMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-06
 */
@Service
public class SpecKeyServiceImpl extends ServiceImpl<SpecKeyMapper, SpecKeyDO> implements SpecKeyService {

    @Autowired
    SpecKeyMapper specKeyMapper;

    @Override
    public List<SpecKeyDO> getBySpuId(Long spuId) {
        return specKeyMapper.getBySpuId(spuId);
    }
}
