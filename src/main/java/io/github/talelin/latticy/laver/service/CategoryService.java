package io.github.talelin.latticy.laver.service;

import io.github.talelin.autoconfigure.exception.NotFoundException;
import io.github.talelin.latticy.laver.model.CategoryDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author generator@TaleLin
 * @since 2021-12-07
 */
public interface CategoryService extends IService<CategoryDO> {

    public CategoryDO getCategory(Long id);
}
