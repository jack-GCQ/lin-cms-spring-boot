package io.github.talelin.latticy.laver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.github.talelin.autoconfigure.exception.NotFoundException;
import io.github.talelin.latticy.common.mybatis.Page;
import io.github.talelin.latticy.laver.bo.BannerWithItemsBO;
import io.github.talelin.latticy.laver.dto.BannerDTO;
import io.github.talelin.latticy.laver.mapper.BannerItemMapper;
import io.github.talelin.latticy.laver.mapper.BannerMapper;
import io.github.talelin.latticy.laver.model.BannerDO;
import io.github.talelin.latticy.laver.model.BannerItemDO;
import io.github.talelin.latticy.laver.service.IBannerService;
import org.omg.CORBA.BAD_CONTEXT;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gcq
 * @Create 2021-11-27
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, BannerDO> implements IBannerService {

    @Autowired
    BannerMapper bannerMapper;
    @Autowired
    BannerItemMapper bannerItemMapper;

    @Override
    public IPage<BannerDO> getBanners(Integer page, Integer count) {
        Page<BannerDO> pager = new Page<>(page, count);
        return this.bannerMapper.selectPage(pager, null);
    }

    @Override
    public void update(Long id, BannerDTO bannerDTO) {
        BannerDO bannerDO = this.baseMapper.selectById(id);
        if(bannerDO == null)
        {
            throw new NotFoundException(20000);
        }
        BeanUtils.copyProperties(bannerDTO, bannerDO);
        this.updateById(bannerDO);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteById(Long id) {
        BannerDO bannerDO = this.baseMapper.selectById(id);
        if(bannerDO == null)
        {
            throw new NotFoundException(20000);
        }
        this.getBaseMapper().deleteById(id);

        // 删除 BannerItem 数据
        LambdaQueryWrapper<BannerItemDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BannerItemDO::getBannerId, id);
        bannerItemMapper.delete(queryWrapper);
    }

    @Override
    public BannerWithItemsBO getWithItems(Long id) {
        BannerDO banner = this.getById(id);
        if(banner == null)
        {
            throw new NotFoundException(20000);
        }

//        QueryWrapper<BannerItemDO> bannerQueryWrapper = new QueryWrapper<>();
//        bannerQueryWrapper.eq("banner_id", id);
//        List<BannerItemDO> bannerItems = bannerItemMapper.selectList(bannerQueryWrapper);
        List<BannerItemDO> bannerItems = new LambdaQueryChainWrapper<>(bannerItemMapper)
                .eq(BannerItemDO::getBannerId, id)
                .list();
        return new BannerWithItemsBO(banner, bannerItems);
    }

    @Override
    public Integer create(BannerDO bannerDO) {
        return this.getBaseMapper().insert(bannerDO);
    }
}