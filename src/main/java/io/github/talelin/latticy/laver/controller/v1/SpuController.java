package io.github.talelin.latticy.laver.controller.v1;


import io.github.talelin.core.annotation.PermissionMeta;
import io.github.talelin.latticy.common.mybatis.Page;
import io.github.talelin.latticy.common.util.PageUtil;
import io.github.talelin.latticy.laver.dto.SpuDTO;
import io.github.talelin.latticy.laver.model.SkuDO;
import io.github.talelin.latticy.laver.model.SpuDetailDO;
import io.github.talelin.latticy.laver.service.SkuService;
import io.github.talelin.latticy.laver.service.SpuService;
import io.github.talelin.latticy.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.github.talelin.latticy.laver.model.SpuDO;

import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import javax.validation.constraints.Positive;

import java.util.List;

/**
* @author generator@TaleLin
* @since 2021-12-04
*/
@RestController
@RequestMapping("/v1/spu")
@Validated
public class SpuController {

    @Autowired
    SpuService spuService;

    @PostMapping("")
    public CreatedVO create(@RequestBody @Validated SpuDTO spuDTO) {
        spuService.create(spuDTO);
        return new CreatedVO();
    }

    @PutMapping("/{id}")
    public UpdatedVO update(@PathVariable @Positive(message = "{id.positive}") Integer id) {
        return new UpdatedVO();
    }

    @DeleteMapping("/{id}")
    public DeletedVO delete(@PathVariable @Positive(message = "{id.positive}") Integer id) {
        return new DeletedVO();
    }

    @GetMapping("/{id}")
    public SpuDO get(@PathVariable(value = "id") @Positive(message = "{id.positive}") Integer id) {
        return null;
    }

    @GetMapping("/{id}/detail")
    public UnifyResponseVO getDetail(@PathVariable("id") Long id)
    {
        SpuDetailDO detail = spuService.getDetail(id);
        return new UnifyResponseVO(detail);
    }


    @GetMapping("/page")
    public PageResponseVO<SpuDO> page(
            @RequestParam(name = "page", required = false, defaultValue = "0")
            @Min(value = 0, message = "{page.number.min}") Integer page,
            @RequestParam(name = "count", required = false, defaultValue = "10")
            @Min(value = 1, message = "{page.count.min}")
            @Max(value = 30, message = "{page.count.max}") Integer count
    ) {
        Page<SpuDO> pager = new Page<>(page, count);
        Page<SpuDO> paging = spuService.getBaseMapper().selectPage(pager, null);
        return PageUtil.build(paging);
    }

}
