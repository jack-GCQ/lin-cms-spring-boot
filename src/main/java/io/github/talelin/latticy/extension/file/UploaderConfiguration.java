package io.github.talelin.latticy.extension.file;

import io.github.talelin.latticy.module.file.Uploader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 文件上传配置类
 *
 * @author Juzi@TaleLin
 * @author colorful@TaleLin
 */
@Configuration(proxyBeanMethods = false)
public class UploaderConfiguration {
    /**
     * @return 本地文件上传实现类
     */
    @Bean
    @Order
    @ConditionalOnMissingBean // 缺少bean为条件
    public Uploader uploader(){
        return new LocalUploader();
    }
    /**
     * 如要上传到其他服务器上 新建相关配置
     */
}
